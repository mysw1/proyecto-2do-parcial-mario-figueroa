/*
author: mario Figueroa
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tictactoe.h"



char ** createArrayContainer(int n){
  char **results;
  results = malloc(n * sizeof *results);
  for (int i=0; i<n; i++)
   results[i] = malloc(n * sizeof *results[i]);
  return results;
}

int main(){
    int n;
    int m;
    int numberofTest;
    scanf("%d", &numberofTest);
    for (int i = 0; i < numberofTest; i++) {
      scanf("%d %d",&n,&m);
      char str[n+1];
      char **results;
      results = createArrayContainer(n);
      for(int a_i = 0; a_i < n; a_i++){
        scanf("%s",str);
         for(int a_j = 0; a_j < n; a_j++){
           results[a_i][a_j]=str[a_j];
           //printf("el char es: %c \n",results[a_i][a_j]);
         }
      }
      if(hasValidInput(results,n,m)){
        int result= getWinner(results,n,m);
        switch (result){
          case 0: printf("DRAW\n");break;
          case 1: printf("X WINS\n");break;
          case 2: printf("O WINS\n");break;
          default:break;
        }
      }
    }
    return 0;
}
