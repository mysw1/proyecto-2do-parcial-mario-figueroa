/*
author: Mario Figueroa
*/
#include <stdio.h>
#include <string.h>
#include "operations.h"

int main(){
  char msg[] = "What is the operation?";
  char * operation_names[] = {
                              "sum",
                              "subtract",
                              "div"
                              };
  float (*operations[])(float,float) = {
                                        sum,
                                        subtract,
                                        div
                                      };
  char operation[10];
  float operator1, operator2, result;
  size_t nelem = sizeof(operation_names)/sizeof(char *);
  int i;

  while(1){

    printf("%s\n",msg );
    scanf("%s",operation);
    tolower_string(operation);
    if(strcmp(operation,"finish")==0){
      break;
    }

    for (i = 0; i < nelem; i++) {
      if(strcmp(operation_names[i],operation)==0)
        break;
    }
    if(i == nelem){
      printf("Operation %s no valid\n",operation );
      continue;
    }

    scanf("%f %f" , &operator1, &operator2);
    printf("Result: %.1f \n", (operations[i])(operator1,operator2));

  }
  return 0;
}
