/*
author: mario Figueroa
*/

#include "tictactoe.h"
#include <string.h>
#include <stdio.h>


bool hasValidInput(char** str, int n ,  int m){
  int counterX=0;
  int counterO=0;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      if(str[i][j]=='X')
        counterX++;
      else if (str[i][j]=='O')
        counterO++;
    }
  }
  if((counterX-counterO)>1 || counterO-counterX>0){
    printf("el valor de diferencia es %d \n",counterX-counterO );
    printf("ERROR\n" );
    return false;
  }
  else if(counterX<m){
    printf("IN PROGRESS\n");
    return false;
  }
  return true;
}


//auxiliar function used on getWinner

int getEast(char** str, int n, int posi, int posj, char globalSearch){
  int counter=0;
  while(posj<n && str[posi][posj]==globalSearch){
      counter ++;
      posj++;
  }
  return counter;
}
//auxiliar function used on getWinner

int getSouth(char** str, int n, int posi, int posj, char globalSearch){
  int counter=0;
  while(posi<n && str[posi][posj]==globalSearch){
      counter ++;
      posi++;
  }
  return counter;
}
//auxiliar function used on getWinner

int getSouthEast(char** str, int n, int posi, int posj, char globalSearch){
  int counter=0;
  while(posj<n && posi<n && str[posi][posj]==globalSearch){
      counter ++;
      posj++;
      posi++;
  }
  return counter;
}
//auxiliar function used on getWinner

int getSouthWest(char** str, int n, int posi, int posj, char globalSearch){
  int counter=0;
  while(posj>=0  && posi<n && str[posi][posj]==globalSearch){
      counter ++;
      posi++;
      posj--;
  }
  return counter;
}


int getWinner(char** str, int n,int m){
  char globalSearch= 'X';
  for (int h = 0; h < 2; h++) {
    if(h>0)globalSearch='O';
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        //check East
        //printf("h:%d i:%d j:%d\n",h,i,j );
        if(getEast(str, n,i,j, globalSearch) >= m){
          if(globalSearch == 'X')return 1;
          else return 2;
        }
        //check South
        if(getSouth(str, n,i,j, globalSearch) >= m){
          if(globalSearch == 'X') return 1;
          else return 2;
        }
        //check SE
        if(getSouthEast(str, n,i,j, globalSearch) >= m){
          if(globalSearch == 'X') return 1;
          else return 2;
        }
        //check SW
        if(getSouthWest(str, n,i,j, globalSearch) >= m){
          if(globalSearch == 'X') return 1;
          else return 2;
        }
      }
    }
  }
  return 0;
}
