/*
author: mario Figueroa
*/
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


#ifndef __TICTACTOE__
#define __TICTACTOE__

//checks if the board is a valid game
//return false if not valid
bool hasValidInput(char** str, int n ,  int m);

//iterates through elements to find the Winner
// returns 1 if X Wins
// returns 2 if O Wins
// returns 0 if DRAW
int getWinner(char** str, int n,int m);


//auxiliar function used on getWinner
int getEast(char** str, int n, int posi, int posj, char globalSearch);
//auxiliar function used on getWinner

int getSouth(char** str, int n, int posi, int posj, char globalSearch);
//auxiliar function used on getWinner

int getSouthEast(char** str, int n, int posi, int posj, char globalSearch);
//auxiliar function used on getWinner

int getSouthWest(char** str, int n, int posi, int posj, char globalSearch);


#endif
