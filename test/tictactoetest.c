/*
author: Mario Figueroa
*/
#include <stdio.h>
#include <stdlib.h>
#include "utest.h"
#include "../src/tictactoe.h"
//

int n; // board size
int m; // number of repetitions needed
char ** str; //2d char array of inputs


static char * test_hasValidInput(){
  n = 4; //grid 3x3
  m = 3; //only win if a line of 3 is achieved
  str = malloc(n * sizeof *str);
  for (int i=0; i<n; i++)
  {
   str[i] = malloc(n * sizeof *str[i]);
  }
  str[0][0]='O';  str[0][1]='X';  str[0][2]='.';  str[0][3]='O';
  str[1][0]='O';  str[1][1]='O';  str[1][2]='X';  str[1][3]='.';
  str[2][0]='.';  str[2][1]='.';  str[2][2]='.';  str[2][3]='.';
  str[3][0]='X';  str[3][1]='O';  str[3][2]='X';  str[3][3]='X';

  bool expectedBool = true;
  bool resultingBool = hasValidInput(str, n ,m);
  assert_test("Board has not valid inputs", resultingBool == expectedBool);
  return 0;
}

static char * test_getWinner(){
  //char *expectedResult= "DRAW";
  int expectedResult=0;
  int resultWinner = getWinner(str, n ,m);
  //printf("%d\n", resultWinner );
  assert_test("Winner result are wrong", resultWinner== expectedResult);
  return 0;
}

int main() {
  run_test("Test hasValidInput:", test_hasValidInput);
  run_test("Test getWinner:", test_getWinner);

  return 0;
}
